<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Fishlog</title>

    <!-- Styles -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <link href="{{ asset('admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/perfectscroll/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/pace/pace.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/highlight/styles/github-gist.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/datatables.min.css') }}" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="{{ asset('admin/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/horizontal-menu/horizontal-menu.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet">

    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo-tab.png') }}"/>

    <script src="{{ asset('admin/plugins/jquery/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/datatables.min.js') }}"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body>
    <div class="app horizontal-menu align-content-stretch d-flex flex-wrap">
        <div class="app-container">
            <div class="search container">
                <form>
                    <input class="form-control" type="text" placeholder="Type here..." aria-label="Search">
                </form>
                <a href="#" class="toggle-search"><i class="material-icons">close</i></a>
            </div>
            <div class="app-header">
                <nav class="navbar navbar-light navbar-expand-lg container">
                    <div class="container-fluid">
                        <div class="navbar-nav" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link hide-sidebar-toggle-button" href="#"><i class="material-icons">last_page</i></a>
                                </li>
                            </ul>
                            <div class="logo">
                                <a href="index.html">Fishlog</a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <ul class="navbar-nav">
                                <li class="nav-item hidden-on-mobile">
                                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="app-menu">
                <div class="container">
                    <ul class="menu-list">
                        <li>
                            <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="#">Master<i class="material-icons has-sub-menu">keyboard_arrow_down</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{ url('admin/master/bank') }}">Bank</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/master/jenis') }}">Jenis</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/master/grade') }}">Grade</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/master/pengguna') }}">Pengguna</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Katalog<i class="material-icons has-sub-menu">keyboard_arrow_down</i></a>
                            <ul class="sub-menu">
                                <?php 
                                    use App\Http\Controllers\HomeController;

                                    $jenis = HomeController::jenis();
                                    foreach($jenis as $item){
                                ?>
                                    <li>
                                        <a href="{{ url('admin/katalog/produk/data/'.$item->id) }}">{{ $item->nama_jenis }}</a>
                                    </li>        
                                <?php
                                    }
                                ?>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ url('admin/transaksi') }}">Transaksi</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="app-content">
                <div class="content-wrapper">
                    <div class="container">

                        @yield('content')

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascripts -->
    <script src="{{ asset('admin/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/perfectscroll/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/highlight/highlight.pack.js') }}"></script>
    
    <script src="{{ asset('admin/js/main.min.js') }}"></script>
    <script src="{{ asset('admin/js/custom.js') }}"></script>
    <script src="{{ asset('admin/js/pages/datatables.js') }}"></script>
    <script src="{{ asset('js/form.js') }}"></script>
</body>

</html>