<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Login | Admin</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo-tab.png') }}"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('logins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('logins/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('logins/assets/css/users/login-1.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <script src="{{ asset('logins/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    
</head>
<body class="login">

    <form class="form-login" method="POST" action="{{ route('Auth.adminLogin') }}">
        @csrf

        <div class="row">
            <div class="col-md-12 text-center mb-4">
                <img alt="logo" src="{{ asset('img/fishlog_logo.svg') }}" class="theme-logo" >
            </div>

            <div class="col-md-12">
                <label for="inputEmail" class="">Nomor Telepon</label>
                <input type="text" name="no_telp" id="inputEmail" class="form-control mb-4" data-mask="9999-9999-9999" required >

                <label for="inputPassword" class="">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control mb-5" required>

                <div class="checkbox d-flex justify-content-between mb-4 mt-3">
                    <div class="custom-control custom-checkbox mr-3">
                        <input type="checkbox" class="custom-control-input" id="customCheck1" value="remember-me">
                        <label class="custom-control-label" for="customCheck1">Remember</label>
                    </div>
                    <div class="forgot-pass">
                        <a href="#">Forgot Password?</a>
                    </div>
                </div>

                <button class="btn btn-lg btn-gradient-primary btn-block btn-rounded mb-4 mt-5" type="submit">Login</button>
                <a href="{{ url('register') }}" class="btn btn-lg btn-gradient-warning btn-block btn-rounded mb-3">Register</a>
            </div>
        </div>
    </form>
    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('logins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('logins/bootstrap/js/bootstrap.min.js') }}"></script>
    
    <!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>