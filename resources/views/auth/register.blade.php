<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Register | Admin</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo-tab.png') }}"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('logins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('logins/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('logins/assets/css/users/login-1.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('logins/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <!-- END GLOBAL MANDATORY STYLES -->

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
</head>
<body class="login">

    <form class="form-login">
        <div class="row">
            <div class="col-md-12 text-center mb-4">
                <img alt="logo" src="{{ asset('img/fishlog_logo.svg') }}" class="theme-logo" >
            </div>

            <div class="col-md-12">
                <label for="inputEmail" class="">Nama</label>
                <input type="text" id="name" class="form-control mb-4">

                <label for="inputEmail" class="">Email</label>
                <input type="email" id="email" class="form-control mb-4">

                <label for="inputEmail" class="">Nomor Telepon</label>
                <input type="text" id="no_telp" class="form-control mb-4" data-mask="9999-9999-9999">

                <label for="inputPassword" class="">Password</label>
                <input type="password" id="password" class="form-control mb-5">

                <label for="inputPassword" class="">Confirm Password</label>
                <input type="password" id="confirm_password" class="form-control mb-5">

                <button class="btn btn-lg btn-gradient-primary btn-block btn-rounded mb-4 mt-5" id="simpan" type="button">Register</button>
                <a href="{{ url('login') }}" class="btn btn-lg btn-gradient-warning btn-block btn-rounded mb-3">Back</a>

            </div>
        </div>
    </form>

    <script>

        $('body').on('click', '#simpan', function (e) {

            var fd;
            fd = new FormData();
            fd.append('name', $('#name').val());
            fd.append('email', $('#email').val());
            fd.append('no_telp', $('#no_telp').val());
            fd.append('password', $('#password').val());
            fd.append('confirm_password', $('#confirm_password').val());
            fd.append('_token', '{{ csrf_token() }}');

            $.ajax({
                data: fd,
                url: "{{ route('Auth.Admin.store') }}",
                type: "POST",
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#SaveModal').html('<span class="spinner-grow spinner-grow-sm" role="status"></span> Loading...');
                    $('#SaveModal').prop("disabled",true);
                },
                success: function (data) {

                    if(data.state == '200'){
                        Swal.fire({
                            text: data.msg,
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.replace("{{ url('login') }}");
                            }
                        })
                    }else{
                        Swal.fire(data.msg, "", "error");
                    }

                },
                error: function (data) {
                    $('#SaveModal').html('Register');
                    $('#SaveModal').prop("disabled",false);

                    console.log(data);
                },
                complete: function() {
                    $('#SaveModal').html('Register');
                    $('#SaveModal').prop("disabled",false);
                }
            });

        });
        
    </script>
    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('logins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('logins/bootstrap/js/bootstrap.min.js') }}"></script>
    
    <!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>