@extends('welcome')

@section('content')

	<div class="row">
        <div class="col">
            <div class="page-description d-flex align-items-center">
                <div class="page-description-content flex-grow-1">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Transaksi</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table id="tbldata" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="20%">No Transaksi</th>
                                <th width="20%">Nama Pemesan</th>
                                <th width="15%">Tanggal</th>
                                <th width="10%">Total</th>
                                <th width="20%">Status</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Begin::Form Input -->
    <div class="modal fade" id="ModalAdd" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: none; margin-top: 100px;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-lg-6"><h5 class="modal-title" id="ModalTitle"></h5></div>
                    <div class="col-lg-6" id="ActionButton" style="text-align: right;"></div>
                </div>
                <div class="modal-body">
                    <form class="row g-3" id="forms">
                        <input type="hidden" id="id">

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">No Transaksi</label>
                            <input type="text" class="form-control" id="no_trx" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">Nama Pemesan</label>
                            <input type="text" class="form-control" id="name" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">No Telp</label>
                            <input type="text" class="form-control" id="no_telp" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">Bank</label>
                            <input type="text" class="form-control" id="nama_bank" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">Status</label>
                            <input type="text" class="form-control" id="status" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">Tanggal Pemesanan</label>
                            <input type="text" class="form-control" id="created_at" readonly>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">Bukti Pembayaran</label>
                            <div id="file_payment"></div>
                        </div>
                        
                        <hr>

                        <table class="table">
                            <thead>
                                <th>Nama Produk</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Total</th>
                            </thead>
                            <tbody id="ListProduk"></tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"><b>TOTAL</b></td>
                                    <td id="TotalCheckout"></td>
                                </tr>
                            </tfoot>
                        </table>

                        <hr>
                    </form>
                </div>
                <div class="modal-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <!-- End::From Input -->

    <div class="modal fade" id="ModalFoto" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: none; margin-top: 100px;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="FilePayment">KESINI</div>
                </div>
            </div>
        </div>
    </div>

    <script>

        var table = null;
        table = $('#tbldata').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            dom: "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7 dataTables_pager'f>>rt<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'p>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 10,
            language: {
                'lengthMenu': 'Tampilkan _MENU_',
            },
            order: [[0, 'asc']],
            ajax: "{{ url('admin/transaksi/getdata') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'no_trx'},
                {data: 'name'},
                {data: 'created_at'},
                {data: 'count'},
                {data: 'status'},
                {data: 'action'}
            ]
            
        });

        $('body').on('click', '.View', function (e) {
            $('#forms').trigger("reset");
            $('#id').val('');
            var id = $(this).data('id');
            $.get("{{ url('admin/transaksi/edit')}}" +"/"+ id,function(data){

                $('#ModalAdd').modal('show');

                $('#id').val(data.data.id);
                $('#no_trx').val(data.data.no_trx);
                $('#name').val(data.data.name);
                $('#no_telp').val(data.data.no_telp);
                $('#nama_bank').val(data.data.nama_bank);
                $('#created_at').val(data.data.created_at);

                $('#file_payment').html('');
                $('#ActionButton').html('');
                $('#ListProduk').html('');
                $('#TotalCheckout').html('');

                if(data.data.status == '1'){
                    $('#status').val('Menunggu pembayaran');
                    $('#ActionButton').html('<button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>');
                }
                if(data.data.status == '2'){
                    $('#status').val('Sudah dibayar');
                    $('#ActionButton').html('<button type="button" class="btn btn-primary" id="simpan">Confirmasi Pembayarn</button>&nbsp;<button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>');
                }
                if(data.data.status == '3'){
                    $('#status').val('Pembayarn Terkonfirmasi');
                    $('#ActionButton').html('<button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>');
                }
                if(data.data.status == '4'){
                    $('#status').val('Selesai');
                    $('#ActionButton').html('<button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>');
                }

                
                $('#file_payment').html('<img style="cursor: pointer;" src="../uploads/payment/'+data.data.file_payment+'" id="ViewFile" class="img-fluid" data-id="'+data.data.file_payment+'">');

                var detail = data.detail;
                var html_list_produk = '';
                var details = detail.map(function(item) {
                    html_list_produk += '<tr>\
                                            <td>'+item.nama_produk+'</td>\
                                            <td>'+item.harga+'</td>\
                                            <td>'+item.qty+'</td>\
                                            <td>'+item.harga * item.qty+'</td>\
                                        </tr>';
                });
                $('#ListProduk').append(html_list_produk);
                $('#TotalCheckout').html(data.data.count);

            });
        });

        $('body').on('click', '#simpan', function (e) {
            var fd;
            fd = new FormData();
            fd.append('id', $('#id').val());
            fd.append('_token', '{{ csrf_token() }}');

            Swal.fire({
                title: "Anda yakin ?",
                icon: "question",
                showCancelButton: true,
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        data: fd,
                        url: "{{ route('Admin.Transaksi.store') }}",
                        type: "POST",
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#simpan').html('<span class="spinner-grow spinner-grow-sm" role="status"></span> Loading...');
                            $('#simpan').prop("disabled",true);
                            $('#close').prop("disabled",true);
                        },
                        success: function (data) {

                            if(data.state == '200'){
                                Swal.fire(data.msg, "", "success");
                                $('#ModalAdd').modal('hide');
                            }else{
                                Swal.fire(data.msg, "", "error");
                            }
                            table.draw();

                        },
                        error: function (data) {
                            $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                            $('#simpan').prop("disabled",false);
                            $('#close').prop("disabled",false);
                            console.log(data);
                        },
                        complete: function() {
                            $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                            $('#simpan').prop("disabled",false);
                            $('#close').prop("disabled",false);
                        }
                    });
                }
            });

        });

        $('body').on('click', '#ViewFile', function (e) {
            var id = $(this).data('id');

            $('#ModalFoto').modal('show');
            $('#FilePayment').html('');
            $('#FilePayment').html('<img src="../uploads/payment/'+id+'" class="img-fluid" >');
        });


    </script>

@endsection