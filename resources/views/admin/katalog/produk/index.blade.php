@extends('welcome')

@section('content')

	<div class="row">
        <div class="col">
            <div class="page-description d-flex align-items-center">
                <div class="page-description-content flex-grow-1">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Katalog</li>
                        <li class="breadcrumb-item" aria-current="page">Produk {{ $jenis->nama_jenis }}</li>
                    </ol>
                </nav>
                </div>
                <div class="page-description-actions">
                    <a href="javascript:void(0)" class="btn btn-primary" id="Add"><i class="material-icons">add</i>Produk {{ $jenis->nama_jenis }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table id="tbldata" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="15%">Nama</th>
                                <th width="10%">Grade</th>
                                <th width="10%">Harga</th>
                                <th width="15%">Foto</th>
                                <th width="15%">Video</th>
                                <th width="20%">Deskripsi</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Begin::Form Input -->
    <div class="modal fade" id="ModalAdd" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: none; margin-top: 100px;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-lg-9"><h5 class="modal-title" id="ModalTitle"></h5></div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-primary" id="simpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </div>
                <div class="modal-body">
                    <form class="row g-3" id="forms">
                        <input type="hidden" id="id">
                        <input type="hidden" id="jenis_id" value="{{ $jenis->id }}">

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="nama">
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Harga</label>
                            <input type="text" class="form-control" id="harga">
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Deskripsi</label>
                            <textarea class="form-control" id="deskripsi"></textarea>
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Grade</label>
                            <select class="form-select" id="grade_id">
                                @foreach($grade as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_grade }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Foto</label>
                            <input type="file" class="form-control" id="foto">
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Video</label>
                            <input type="file" class="form-control" id="video">
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <!-- End::From Input -->

    <script>

        var table = null;
        table = $('#tbldata').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            dom: "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7 dataTables_pager'f>>rt<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'p>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 10,
            language: {
                'lengthMenu': 'Tampilkan _MENU_',
            },
            order: [[0, 'asc']],
            ajax: "{{ url('admin/katalog/produk/getdata') }}" +"/{{ $jenis->id }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'nama'},
                {data: 'grade'},
                {data: 'harga'},
                {data: 'foto'},
                {data: 'video'},
                {data: 'deskripsi'},
                {data: 'action'}
            ]
            
        });

        $('body').on('click', '#Add', function (e) {
            $('#ModalAdd').modal('show');
            $('#ModalTitle').html('Add');
            $('#forms').trigger("reset");
            $('#id').val('');
        });

        $('body').on('click', '.Edit', function (e) {
            $('#forms').trigger("reset");
            $('#id').val('');
            var id = $(this).data('id');
            $.get("{{ url('admin/katalog/produk/edit')}}" +"/"+ id,function(data){

                $('#ModalAdd').modal('show');

                $('#id').val(data.id);
                $('#nama').val(data.nama);
                $('#harga').val(data.harga);
                $('#deskripsi').val(data.deskripsi);
                $('#grade_id').val(data.grade_id);

            });
        });

        $('body').on('click', '#simpan', function (e) {
            var fd;
            fd = new FormData();
            fd.append('id', $('#id').val());
            fd.append('nama', $('#nama').val());
            fd.append('harga', $('#harga').val());
            fd.append('deskripsi', $('#deskripsi').val());
            fd.append('grade_id', $('#grade_id').val());
            fd.append('jenis_id', $('#jenis_id').val());

            if($('#foto').get(0).files.length > 0){
                fd.append('foto', $('#foto').prop('files')[0]);
            }else{
                fd.append('foto', '');
            }

            if($('#video').get(0).files.length > 0){
                fd.append('video', $('#video').prop('files')[0]);
            }else{
                fd.append('video', '');
            }

            fd.append('_token', '{{ csrf_token() }}');

            $.ajax({
                data: fd,
                url: "{{ route('Admin.Katalog.Produk.store') }}",
                type: "POST",
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#simpan').html('<span class="spinner-grow spinner-grow-sm" role="status"></span> Loading...');
                    $('#simpan').prop("disabled",true);
                    $('#close').prop("disabled",true);
                },
                success: function (data) {

                    if(data.state == '200'){
                        Swal.fire(data.msg, "", "success");
                        $('#ModalAdd').modal('hide');
                    }else{
                        Swal.fire(data.msg, "", "error");
                    }
                    table.draw();

                },
                error: function (data) {
                    $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                    $('#simpan').prop("disabled",false);
                    $('#close').prop("disabled",false);
                    console.log(data);
                },
                complete: function() {
                    $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                    $('#simpan').prop("disabled",false);
                    $('#close').prop("disabled",false);
                }
            });

        });

        $('body').on('click', '.Delete', function (e) {
            var id = $(this).data('id');
            Swal.fire({
                title: "Anda yakin ?",
                icon: "question",
                showCancelButton: true,
            }).then(function(result) {
                if (result.value) {
                    var fd;
                    fd = new FormData();
                    fd.append('id', id);
                    fd.append('_token', '{{ csrf_token() }}');

                    $.ajax({
                        data: fd,
                        url: "{{ route('Admin.Katalog.Produk.destroy') }}",
                        type: "POST",
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data) {

                            if(data.state == '200'){
                                Swal.fire(data.msg, "", "success");
                            }else{
                                Swal.fire(data.msg, "", "error");
                            }
                            table.draw();

                        },
                        error: function (data) {
                            console.log(data);
                        },
                        complete: function() {
                        }
                    });
                }
            });
        });


        
    </script>

@endsection