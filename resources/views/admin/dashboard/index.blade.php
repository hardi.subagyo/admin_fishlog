@extends('welcome')

@section('content')

	<div class="row">
        <div class="col">
            <div class="page-description">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-xl-6">
                    <div class="card widget widget-list">
                        <div class="card-header">
                            <h5 class="card-title">Jumlah Produk</h5>
                        </div>
                        <div class="card-body">
                            <ul class="widget-list-content list-unstyled">
                                @foreach($arr_jenis as $jenis)
                                    <li class="widget-list-item widget-list-item-blue">
                                        <span class="widget-list-item-icon">
                                            <i class="material-icons-outlined">receipt</i>
                                        </span>
                                        <span class="widget-list-item-description">
                                            <a href="#" class="widget-list-item-description-title">
                                                {{ $jenis['jenis'] }}
                                            </a>
                                        </span>
                                        <span class="widget-list-item-transaction-amount-negative">{{ number_format($jenis['count'],0,'.','.') }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6">
                    <div class="card widget widget-list">
                        <div class="card-header">
                            <h5 class="card-title">Riwayat Pemesanan</h5>
                        </div>
                        <div class="card-body">
                            <ul class="widget-list-content list-unstyled">
                                @foreach($transaksi as $trx)
                                    <li class="widget-list-item widget-list-item-blue">
                                        <span class="widget-list-item-icon">
                                            <i class="material-icons-outlined">receipt</i>
                                        </span>
                                        <span class="widget-list-item-description">
                                            <a href="#" class="widget-list-item-description-title">
                                                {{ $trx->name }}
                                            </a>
                                            <span class="widget-list-item-description-date">
                                                {{ $trx->created_at }}
                                            </span>
                                        </span>
                                        <span class="widget-list-item-transaction-amount-negative">Rp. {{ number_format($trx->count,0,'.','.') }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection