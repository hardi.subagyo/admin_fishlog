@extends('welcome')

@section('content')

	<div class="row">
        <div class="col">
            <div class="page-description d-flex align-items-center">
                <div class="page-description-content flex-grow-1">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Master</li>
                            <li class="breadcrumb-item" aria-current="page">Pengguna</li>
                            <li class="breadcrumb-item" aria-current="page">{{ $data->name }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Detail Pengguna</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">Nama</label>
                            <input type="text" class="form-control" readonly value="{{ $data->name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">Email</label>
                            <input type="text" class="form-control" readonly value="{{ $data->email }}">
                        </div>
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">No Telp</label>
                            <input type="text" class="form-control" readonly value="{{ $data->no_telp }}">
                        </div>
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">Jumlah Frekuensi Pesanan</label>
                            <input type="text" class="form-control" readonly value="">
                        </div>
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">Total Belanja</label>
                            <input type="text" class="form-control" readonly value="Rp. {{ number_format($totalbelanja,0,'.','.') }}">
                        </div>
                        <div class="col-md-6">
                            <label for="settingsPhoneNumber" class="form-label">Foto</label>
                            <br>
                            <img src="{{ asset('uploads/foto/pengguna/'.$data->foto) }}" class="img-thumbnail">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Riwayat Pesanan</h5>
                </div>
                <div class="card-body">
                    <table id="tbldata" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="25%">Nama</th>
                                <th width="25%">Harga</th>
                                <th width="25%">Jumlah</th>
                                <th width="20%">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach($riwayat as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nama_produk }}</td>
                                    <td>{{ number_format($item->harga,0,'.','.') }}</td>
                                    <td>{{ number_format($item->qty,0,'.','.') }}</td>
                                    <td>{{ number_format($item->harga * $item->qty,0,'.','.') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>

        var table = null;
        table = $('#tbldata').DataTable();

    </script>

@endsection