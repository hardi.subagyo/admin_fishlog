@extends('welcome')

@section('content')

	<div class="row">
        <div class="col">
            <div class="page-description d-flex align-items-center">
                <div class="page-description-content flex-grow-1">
                    <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Master</li>
                        <li class="breadcrumb-item" aria-current="page">Grade</li>
                    </ol>
                </nav>
                </div>
                <div class="page-description-actions">
                    <a href="javascript:void(0)" class="btn btn-primary" id="Add"><i class="material-icons">add</i>Grade</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table id="tbldata" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="25%">Jenis</th>
                                <th width="25%">Nama Grade</th>
                                <th width="25%">Deskripsi</th>
                                <th width="20%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Begin::Form Input -->
    <div class="modal fade" id="ModalAdd" tabindex="-1" aria-labelledby="exampleModalCenterTitle" style="display: none; margin-top: 100px;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalTitle"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="row g-3" id="forms">
                        <input type="hidden" id="id">

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Jenis</label>
                            <select class="form-control" id="jenis_id">
                                @foreach($jenis as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_jenis }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Nama Grade</label>
                            <input type="text" class="form-control" id="nama_grade">
                        </div>

                        <div class="col-12">
                            <label for="inputAddress" class="form-label">Deskripsi Grade</label>
                            <textarea class="form-control" id="deskripsi_grade"></textarea>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="close" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="simpan">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End::From Input -->

    <script>

        var table = null;
        table = $('#tbldata').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            dom: "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7 dataTables_pager'f>>rt<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'p>>",
            lengthMenu: [5, 10, 25, 50],
            pageLength: 10,
            language: {
                'lengthMenu': 'Tampilkan _MENU_',
            },
            order: [[0, 'asc']],
            ajax: "{{ url('admin/master/grade/getdata') }}",
            columns: [
                {data: 'DT_RowIndex'},
                {data: 'nama_jenis'},
                {data: 'nama_grade'},
                {data: 'deskripsi_grade'},
                {data: 'action'}
            ]
            
        });

        $('body').on('click', '#Add', function (e) {
            $('#ModalAdd').modal('show');
            $('#ModalTitle').html('Add');
            $('#forms').trigger("reset");
            $('#id').val('');
        });

        $('body').on('click', '.Edit', function (e) {
            $('#forms').trigger("reset");
            $('#id').val('');
            var id = $(this).data('id');
            $.get("{{ url('admin/master/grade/edit')}}" +"/"+ id,function(data){

                $('#ModalAdd').modal('show');

                $('#id').val(data.id);
                $('#nama_grade').val(data.nama_grade);
                $('#deskripsi_grade').val(data.deskripsi_grade);
                $('#jenis_id').val(data.jenis_id);

            });
        });

        $('body').on('click', '#simpan', function (e) {
            var fd;
            fd = new FormData();
            fd.append('id', $('#id').val());
            fd.append('nama_grade', $('#nama_grade').val());
            fd.append('deskripsi_grade', $('#deskripsi_grade').val());
            fd.append('jenis_id', $('#jenis_id').val());
            fd.append('_token', '{{ csrf_token() }}');

            $.ajax({
                data: fd,
                url: "{{ route('Admin.Master.Grade.store') }}",
                type: "POST",
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#simpan').html('<span class="spinner-grow spinner-grow-sm" role="status"></span> Loading...');
                    $('#simpan').prop("disabled",true);
                    $('#close').prop("disabled",true);
                },
                success: function (data) {

                    if(data.state == '200'){
                        Swal.fire(data.msg, "", "success");
                        $('#ModalAdd').modal('hide');
                    }else{
                        Swal.fire(data.msg, "", "error");
                    }
                    table.draw();

                },
                error: function (data) {
                    $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                    $('#simpan').prop("disabled",false);
                    $('#close').prop("disabled",false);
                    console.log(data);
                },
                complete: function() {
                    $('#simpan').html('<i class="icon-sm fas fa-save"></i> Simpan');
                    $('#simpan').prop("disabled",false);
                    $('#close').prop("disabled",false);
                }
            });

        });

        $('body').on('click', '.Delete', function (e) {
            var id = $(this).data('id');
            Swal.fire({
                title: "Anda yakin ?",
                icon: "question",
                showCancelButton: true,
            }).then(function(result) {
                if (result.value) {
                    var fd;
                    fd = new FormData();
                    fd.append('id', id);
                    fd.append('_token', '{{ csrf_token() }}');

                    $.ajax({
                        data: fd,
                        url: "{{ route('Admin.Master.Grade.destroy') }}",
                        type: "POST",
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data) {

                            if(data.state == '200'){
                                Swal.fire(data.msg, "", "success");
                            }else{
                                Swal.fire(data.msg, "", "error");
                            }
                            table.draw();

                        },
                        error: function (data) {
                            console.log(data);
                        },
                        complete: function() {
                        }
                    });
                }
            });
        });


    </script>

@endsection