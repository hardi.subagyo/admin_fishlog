<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\AuthController@authenticate');
Route::post('register', 'Api\RegisterController@register');

Route::group(['middleware' => ['jwt.verify']], function() {

	Route::get('getuser', 'Api\User\UserController@getuser');
	Route::get('logout', 'Api\AuthController@logout');

	/*KERANJANG*/
	Route::post('keranjang', 'Api\Keranjang\KeranjangController@store');
	Route::get('list_keranjang', 'Api\Keranjang\KeranjangController@list_keranjang');

	/*KATALOG*/
	Route::get('katalog', 'Api\Katalog\ProdukController@getdata');

	/*CHECKOUT*/
	Route::post('checkout', 'Api\Checkout\CheckoutController@store');
	Route::get('list_checkout', 'Api\Checkout\CheckoutController@list');
	Route::get('detail_checkout', 'Api\Checkout\CheckoutController@detail');

	/*CONFIRM*/
	Route::post('confirm', 'Api\Confirm\ConfirmController@store');

});