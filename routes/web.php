<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/jenis', 'HomeController@jenis')->name('jenis');

Route::name('Auth.')->namespace('Auth')->prefix('auth')->group(function () {

	Route::name('Admin.')->prefix('admin')->group(function () {
		Route::post('store', 'AdminsController@store')->name('store');
	});
	
	Route::post('adminLogin', 'LoginController@adminLogin')->name('adminLogin');

});

Route::group(['middleware' => 'auth:admin'], function () {

	Route::name('Admin.')->namespace('Admin')->prefix('admin')->group(function () {

		Route::name('Dashboard.')->namespace('Dashboard')->prefix('dashboard')->group(function () {
			Route::get('/', 'DashboardController@index')->name('index');
		});

		Route::name('Master.')->namespace('Master')->prefix('master')->group(function () {

			#Jenis
			Route::name('Jenis.')->namespace('Jenis')->prefix('jenis')->group(function () {
				Route::get('/', 'JenisController@index')->name('index');
				Route::get('/getdata', 'JenisController@getdata')->name('getdata');
				Route::get('/edit/{id}', 'JenisController@edit')->name('edit');
				Route::post('store', 'JenisController@store')->name('store');
				Route::post('destroy', 'JenisController@destroy')->name('destroy');
			});

			#Grade
			Route::name('Grade.')->namespace('Grade')->prefix('grade')->group(function () {
				Route::get('/', 'GradeController@index')->name('index');
				Route::get('/getdata', 'GradeController@getdata')->name('getdata');
				Route::get('/edit/{id}', 'GradeController@edit')->name('edit');
				Route::post('store', 'GradeController@store')->name('store');
				Route::post('destroy', 'GradeController@destroy')->name('destroy');
			});

			#Pengguna
			Route::name('Pengguna.')->namespace('Pengguna')->prefix('pengguna')->group(function () {
				Route::get('/', 'PenggunaController@index')->name('index');
				Route::get('/getdata', 'PenggunaController@getdata')->name('getdata');
				Route::get('/edit/{id}', 'PenggunaController@edit')->name('edit');
				Route::get('/show/{id}', 'PenggunaController@show')->name('show');
				Route::post('store', 'PenggunaController@store')->name('store');
				Route::post('destroy', 'PenggunaController@destroy')->name('destroy');
			});

			#Bank
			Route::name('Bank.')->namespace('Bank')->prefix('bank')->group(function () {
				Route::get('/', 'BankController@index')->name('index');
				Route::get('/getdata', 'BankController@getdata')->name('getdata');
				Route::get('/edit/{id}', 'BankController@edit')->name('edit');
				Route::post('store', 'BankController@store')->name('store');
			});

		});

		Route::name('Katalog.')->namespace('Katalog')->prefix('katalog')->group(function () {

			#
			Route::name('Produk.')->namespace('Produk')->prefix('produk')->group(function () {
				Route::get('/data/{id}', 'ProdukController@index')->name('index');
				Route::get('/getdata/{id}', 'ProdukController@getdata')->name('getdata');
				Route::get('/edit/{id}', 'ProdukController@edit')->name('edit');
				Route::post('store', 'ProdukController@store')->name('store');
				Route::post('destroy', 'ProdukController@destroy')->name('destroy');
			});

		});

		#Jenis
		Route::name('Transaksi.')->namespace('Transaksi')->prefix('transaksi')->group(function () {
			Route::get('/', 'TransaksiController@index')->name('index');
			Route::get('/getdata', 'TransaksiController@getdata')->name('getdata');
			Route::get('/edit/{id}', 'TransaksiController@edit')->name('edit');
			Route::post('store', 'TransaksiController@store')->name('store');
		});

	});

});
