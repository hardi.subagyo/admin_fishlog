<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_trx');
            $table->integer('produk_id');
            $table->string('nama_produk');
            $table->string('foto');
            $table->integer('harga');
            $table->integer('grade_id');
            $table->string('nama_grade');
            $table->integer('jenis_id');
            $table->string('nama_jenis');
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_detail');
    }
}
