<?php

namespace App\Models\Katalog;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Produk extends Model
{
    protected $table = 'produk';

    protected $fillable = [
        'foto','video','nama','deskripsi','harga','grade_id','jenis_id'
    ];

    public function grade(){
    	return $this->belongsTo('App\Models\Master\Grade','grade_id','id');
    }

    public function jenis(){
    	return $this->belongsTo('App\Models\Master\Jenis','jenis_id','id');
    }
}
