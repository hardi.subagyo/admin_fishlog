<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class TrxCheckout extends Model
{
    protected $table = 'trx_checkout';

    protected $fillable = [
        'bank_id','users_id','no_trx','count','status','expired_checkout','file_payment'
    ];
}
