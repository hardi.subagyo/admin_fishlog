<?php

namespace App\Models\Trx;

use Illuminate\Database\Eloquent\Model;

class TrxDetail extends Model
{
    protected $table = 'trx_detail';

    protected $fillable = [
        'no_trx','produk_id','nama_produk','foto','harga','grade_id','nama_grade','jenis_id','nama_jenis','qty'
    ];
}
