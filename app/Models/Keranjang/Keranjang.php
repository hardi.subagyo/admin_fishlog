<?php

namespace App\Models\Keranjang;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use App\Models\Katalog\Produk;
use App\Models\Master\Grade;

class Keranjang extends Model
{
    protected $table = 'keranjang';

    protected $fillable = [
        'users_id','produk_id','qty','jenis'
    ];
    
}
