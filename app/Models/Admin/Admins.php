<?php

namespace App\Models\Admin;

/*use Illuminate\Database\Eloquent\Model;*/
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admins extends Authenticatable
{

	use Notifiable;

    protected $table = 'admins';

    protected $fillable = [
        'name','email','no_telp','password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}

