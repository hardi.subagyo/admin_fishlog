<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grade';

    protected $fillable = [
        'nama_grade','deskripsi_grade','jenis_id'
    ];

    public function jenis(){
    	return $this->belongsTo('App\Models\Master\Jenis','jenis_id','id');
    }
}
