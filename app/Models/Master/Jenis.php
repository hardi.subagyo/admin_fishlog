<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = 'jenis';

    protected $fillable = [
        'nama_jenis'
    ];
}
