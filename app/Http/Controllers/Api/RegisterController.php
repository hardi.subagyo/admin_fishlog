<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Api\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
	public function register(Request $request)
    {
    	$validator = Validator::make($request->all(), [
		    'name' => 'required',
		    'email' => 'required|email|unique:users',
		    'no_telp' => 'required|unique:users',
		    'password' => 'required|min:6|max:50|same:confirm_password',
		    'confirm_password' => 'required|min:6',
		    'foto' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
		]); 

        if ($validator->fails()) {
            return response()->json([
	            'success' => false,
	            'message' => 'Error',
	            'data' => $validator->messages()
	        ], Response::HTTP_OK);
        }

        $user = array(
            'name' => $request->name,
            'email' => $request->email,
            'no_telp' => str_replace('-','',$request->no_telp),
            'password' => bcrypt($request->password)
        );

        if(!empty($request->file('foto'))){
        	$foto = str_replace(" ","_",$request->name).'-'.date('Ymdhis').'-'.str_replace(" ","_", $request->file('foto')->getClientOriginalName());
            $request->file('foto')->move(public_path('uploads/foto/pengguna'), $foto);
            $user['foto'] = $foto;
        }else{
        	$user['foto'] = '';
        }

        $insert = User::create($user);

        if($insert){
            return response()->json([
	            'success' => true,
	            'message' => 'User created successfully',
	            'data' => $insert
	        ], Response::HTTP_OK);
        }else{
            return response()->json([
	            'success' => false,
	            'message' => 'User created failed',
	            'data' => $insert
	        ], Response::HTTP_OK);
        }
    }
}
