<?php

namespace App\Http\Controllers\Api\Keranjang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Keranjang\Keranjang;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use DB;

class KeranjangController extends Controller
{
    protected $user;

    public function store(Request $request)
    {
    	try {

			if ($user = JWTAuth::parseToken()->authenticate()) {

				$keranjang = array(
		            'users_id' => $user->id,
		            'produk_id' => $request->produk_id,
		            'qty' => $request->qty
		        );

		        $insert = Keranjang::create($keranjang);

		        if($insert){
		            return response()->json([
			            'success' => true,
			            'message' => 'Success',
			            'data' => $insert
			        ], Response::HTTP_OK);
		        }else{
		            return response()->json([
			            'success' => false,
			            'message' => 'Failed',
			            'data' => []
			        ], Response::HTTP_OK);
		        }
				
			}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['failed'], $e->getStatusCode());

		}
    }

    public function list_keranjang()
    {
    	try {

			if ($user = JWTAuth::parseToken()->authenticate()) {

				$list = DB::table('keranjang as k')
							->select(
								'k.id',
								'k.qty',
								'p.nama',
								'p.harga'
							)
							->join('produk as p','p.id','=','k.produk_id')
							->join('grade as g','g.id','=','p.grade_id')
							->join('jenis as j','j.id','=','p.jenis_id')
							->where('k.users_id',$user->id)
							->orderby('p.nama','asc')
							->get();

				return response()->json([
                    'success' => true,
                    'message' => 'Success',
                    'data' => $list
                ], Response::HTTP_OK);

			}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['failed'], $e->getStatusCode());

		}
    }
}
