<?php

namespace App\Http\Controllers\Api\Katalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Models\Katalog\Produk;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ProdukController extends Controller
{
    protected $user;
 
    public function getdata(Request $request)
    {
        $jenis_id = $request->input('jenis_id');
        $nama = $request->input('nama');

        try {

            if ($user = JWTAuth::parseToken()->authenticate()) {

                if(empty($nama)){
                    $list = Produk::with('grade')->where('jenis_id',$jenis_id)->orderby('nama','asc')->paginate(6);
                }else{
                    $list = Produk::with('grade')->where('jenis_id',$jenis_id)->where('nama','LIKE','%'.$nama.'%')->orderby('nama','asc')->paginate(6);
                }

                return response()->json([
                    'success' => true,
                    'message' => 'Success',
                    'data' => $list
                ], Response::HTTP_OK);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['failed'], $e->getStatusCode());

        }
    }


}
