<?php

namespace App\Http\Controllers\Api\Confirm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use File;
use App\Models\Trx\TrxCheckout;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ConfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            if ($user = JWTAuth::parseToken()->authenticate()) {

                $validator = Validator::make($request->all(), [
                    'file_payment' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'success' => false,
                        'message' => $validator->errors()->first(),
                        'data' => []
                    ], Response::HTTP_OK);
                }else{

                    $getdate = TrxCheckout::where('no_trx', $request->no_trx)->first();

                    if(date('Y-m-d H:i:s') < $getdate->expired_checkout)
                    {
                        if($getdate->status == '3' || $getdate->status == '4'){
                            return response()->json([
                                'success' => false,
                                'message' => 'Pesanan tidak dapat dikonfirmasi kembali',
                                'data' => []
                            ], Response::HTTP_OK);
                        }else{

                            $foto = str_replace(" ","_",$user->name).'-'.date('Ymdhis').'-'.str_replace(" ","_", $request->file('file_payment')->getClientOriginalName());
                            $request->file('file_payment')->move(public_path('uploads/payment'), $foto);

                            $update = TrxCheckout::where('no_trx', $request->no_trx)
                                        ->update(
                                            [
                                                'status' => 2,
                                                'file_payment' => $foto
                                            ]
                                        );

                            if($update){
                                return response()->json([
                                    'success' => true,
                                    'message' => 'Success',
                                    'data' => $update
                                ], Response::HTTP_OK);
                            }else{
                                return response()->json([
                                    'success' => false,
                                    'message' => 'Failed',
                                    'data' => []
                                ], Response::HTTP_OK);
                            }

                        }
                    }else{
                        return response()->json([
                                'success' => false,
                                'message' => 'Expired date checkout',
                                'data' => []
                            ], Response::HTTP_OK);
                    }

                }
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['failed'], $e->getStatusCode()); 

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
