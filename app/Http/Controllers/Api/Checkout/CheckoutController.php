<?php

namespace App\Http\Controllers\Api\Checkout;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use App\Models\Trx\TrxCheckout;
use App\Models\Trx\TrxDetail;
use App\Models\Keranjang\Keranjang;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class CheckoutController extends Controller
{
    protected $user;

    public function store(Request $request)
    {
        try {

            if ($user = JWTAuth::parseToken()->authenticate()) {

            	$list = $request->id_keranjang;
            	$notrx = 'TRX/'.date('Ymd').'/'.$user->id.'/'.strtoupper(substr($user->name,0,3)).'/'.date('his');

            	$data = array();
                $count = 0;
            	foreach(json_decode($list) as $item){

                    $getdata = DB::table('keranjang as k')
                                    ->select(
                                        'k.produk_id',
                                        'p.nama as nama_produk',
                                        'p.foto',
                                        'p.harga',
                                        'p.grade_id',
                                        'g.nama_grade',
                                        'p.jenis_id',
                                        'j.nama_jenis',
                                        'k.qty'
                                    )
                                    ->join('produk as p','p.id','=','k.produk_id')
                                    ->join('grade as g','g.id','=','p.grade_id')
                                    ->join('jenis as j','j.id','=','p.jenis_id')
                                    ->where('k.id',$item)
                                    ->where('k.users_id',$user->id)
                                    ->first();

                    $count += $getdata->harga * $getdata->qty;

                    TrxDetail::create([
                        'no_trx' => $notrx,
                        'produk_id' => $getdata->produk_id,
                        'nama_produk' => $getdata->nama_produk,
                        'foto' => $getdata->foto,
                        'harga' => $getdata->harga,
                        'grade_id' => $getdata->grade_id,
                        'nama_grade' => $getdata->nama_grade,
                        'jenis_id' => $getdata->jenis_id,
                        'nama_jenis' => $getdata->nama_jenis,
                        'qty' => $getdata->qty
                    ]);

                    Keranjang::find($item)->delete();

            	}

                $insert = TrxCheckout::create([
                    'bank_id' => $request->bank_id,
                    'users_id' => $user->id,
                    'no_trx' => $notrx,
                    'count' => $count,
                    'status' => 1,
                    'expired_checkout' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +1 day'))
                ]);

                if($insert){
                    return response()->json([
                        'success' => true,
                        'message' => 'Success',
                        'data' => $insert
                    ], Response::HTTP_OK);
                }else{
                    return response()->json([
                        'success' => false,
                        'message' => 'Failed',
                        'data' => []
                    ], Response::HTTP_OK);
                }
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['failed'], $e->getStatusCode());

        }
    }


    public function list()
    {
        try {

            if ($user = JWTAuth::parseToken()->authenticate()) {

                $list = TrxCheckout::where('users_id',$user->id)->get();

                return response()->json([
                    'success' => true,
                    'message' => 'Success',
                    'data' => $list
                ], Response::HTTP_OK);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['failed'], $e->getStatusCode());

        }
    }

    public function detail(Request $request)
    {
        try {

            if ($user = JWTAuth::parseToken()->authenticate()) {

                $list = TrxDetail::where('no_trx',$request->no_trx)
                    ->orderBy('nama_produk','asc')
                    ->get();

                return response()->json([
                    'success' => true,
                    'message' => 'Success',
                    'data' => $list
                ], Response::HTTP_OK);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['failed'], $e->getStatusCode());

        }   
    }

}
