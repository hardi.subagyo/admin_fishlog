<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Admins;
use Illuminate\Support\Facades\Hash;
use Validator;

class AdminsController extends Controller
{
    public function store(Request $request)
    {

    	$validator = Validator::make($request->all(), [
	        'name' => 'required',
	        'email' => 'required|email|unique:admins',
	        'no_telp' => 'required|unique:admins',
	        'password' => 'required|min:6|max:50|same:confirm_password',
	        'confirm_password' => 'required|min:6',
	    ]);

	    if ($validator->fails()) {

	        $output = array(
	    		'state' => '500',
			    'msg' => $validator->errors()->first(),
	    	);
	    }else{

	    	$data = array(
	    		'name' => $request->name,
		        'email' => $request->email,
		        'no_telp' => str_replace('-','',$request->no_telp),
		        'password' => Hash::make($request->password)
	    	);

	    	$insert = Admins::updateOrCreate($data);

            if($insert){
                $output = array(
                    'state' => 200,
                    'msg' => 'Sukses'
                );
            }else{
                $output = array(
                    'state' => 500,
                    'msg' => 'Gagal'
                );
            }


	    	$output = array(
	    		'state' => '200',
			    'msg' => 'success',	
	    	);

	    }

    	

    	return response()->json($output);
    }
}
