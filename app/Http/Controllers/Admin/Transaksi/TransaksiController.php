<?php

namespace App\Http\Controllers\Admin\Transaksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Trx\TrxCheckout;
use App\Models\Trx\TrxDetail;
use Validator;
use DataTables;
use File;
use DB;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.transaksi.index');
    }

    public function getdata()
    {
        $data = DB::table('trx_checkout as trx')
                    ->select('trx.*','u.name','u.no_telp')
                    ->join('users as u','u.id','=','trx.users_id')
                    ->orderBy('trx.id','desc')
                    ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<button type="button" class="btn btn-info btn-sm View" data-id="' . $row->id . '">
                            <i class="material-icons">visibility</i>
                        </button>
                        ';
                return $btn;
            })
            ->addColumn('status', function ($row) {

                if($row->status == '1'){
                    $status = 'Menunggu pembayaran';
                }else if($row->status == '2'){
                    $status = 'Sudah dibayar';
                }else if($row->status == '3'){
                    $status = 'Pembayarn Terkonfirmasi';
                }else if($row->status == '4'){
                    $status = 'Selesai';
                }

                return $status;
            })
            ->addColumn('count', function ($row) {

                return "Rp. ".number_format($row->count,0,'.','.');
            })
            ->rawColumns(['action','status','count'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'status' => '3'
        );

        $insert = TrxCheckout::updateOrCreate(
            [
                'id' => $request->id
            ],
            $data
        );

        if($insert){
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );
        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('trx_checkout as trx')
                    ->select('trx.*','u.name','u.no_telp','b.nama_bank')
                    ->join('users as u','u.id','=','trx.users_id')
                    ->join('bank as b','b.id','=','trx.bank_id')
                    ->where('trx.id',$id)
                    ->first();

        $detail = array();
        $getdetail = TrxDetail::where('no_trx',$data->no_trx)
                    ->orderBy('nama_produk','asc')
                    ->get();
        foreach($getdetail as $item)
        {
            array_push($detail, $item);
        }

        $output = array(
            'data' => $data,
            'detail' => $detail
        );

        return response()->json($output);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
