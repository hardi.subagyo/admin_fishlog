<?php

namespace App\Http\Controllers\Admin\Master\Pengguna;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\User;
use App\Models\Trx\TrxCheckout;
use App\Models\Trx\TrxDetail;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Validator;
use File;
use DB;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.pengguna.index');
    }

    public function getdata()
    {
        $data = User::all();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<button type="button" class="btn btn-primary btn-sm View" data-id="' . $row->id . '">
                            <i class="material-icons">arrow_right_alt</i>
                        </button>
                        <button type="button" class="btn btn-warning btn-sm Edit" data-id="' . $row->id . '">
                            <i class="material-icons">mode_edit_outline</i>
                        </button>
                        <button type="button" class="btn btn-sm  btn-danger Delete" data-id="' . $row->id . '">
                            <i class="material-icons">restore_from_trash</i>
                        </button>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->id == ''){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'no_telp' => 'required|unique:users',
                'password' => 'required|min:6|max:50|same:confirm_password',
                'confirm_password' => 'required|min:6',
                'foto' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);    
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'no_telp' => 'required',
                'password' => 'nullable|min:6|max:50|same:confirm_password',
                'confirm_password' => 'nullable|min:6',
                'foto' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);
        }
        

        if ($validator->fails()) {

            $output = array(
                'state' => '500',
                'msg' => $validator->errors()->first(),
            );
        }else{

            $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'no_telp' => str_replace('-','',$request->no_telp),
                'password' => Hash::make($request->password)
            );

            if ($request->file('foto') != '') {

                if($request->id != ''){
                    $getdata = User::find($request->id);
                    if($getdata->foto != ""){
                        File::delete(public_path("uploads/foto/pengguna/".$getdata->foto));
                    }
                }

                $foto = date('ymdhis').'-'.str_replace(" ","_", $request->file('foto')->getClientOriginalName());
                $request->file('foto')->move(public_path('uploads/foto/pengguna'), $foto);
                $data['foto'] = $foto;
            }

            $insert = User::updateOrCreate(
                [
                    'id' => $request->id
                ],
                $data
            );

            if($insert){
                $output = array(
                    'state' => 200,
                    'msg' => 'Sukses'
                );
            }else{
                $output = array(
                    'state' => 500,
                    'msg' => 'Gagal'
                );
            }


            $output = array(
                'state' => '200',
                'msg' => 'success', 
            );

        }

        echo json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::find($id);
        $totalbelanja = TrxCheckout::where("users_id",$id)->sum('count');
        $riwayat = DB::table('trx_detail as d')
                        ->select('d.*')
                        ->join('trx_checkout as c','c.no_trx','=','d.no_trx')
                        ->where('c.users_id',$id)
                        ->get();

        return view('admin.master.pengguna.show',compact(['data','totalbelanja','riwayat']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = User::find($request->id);

        File::delete(public_path("uploads/foto/pengguna/".$data->foto));

        $delete = User::find($request->id)->delete();

        if($delete)
        {
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );

        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }
}
