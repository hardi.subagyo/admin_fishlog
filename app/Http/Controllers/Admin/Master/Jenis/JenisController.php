<?php

namespace App\Http\Controllers\Admin\Master\Jenis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Master\Jenis;


class JenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.jenis.index');
    }

    public function getdata()
    {
        $data = Jenis::orderby('nama_jenis','asc')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<button type="button" class="btn btn-warning btn-sm Edit" data-id="' . $row->id . '">
                            <i class="material-icons">mode_edit_outline</i>
                        </button>
                        <button type="button" class="btn btn-sm  btn-danger Delete" data-id="' . $row->id . '">
                            <i class="material-icons">restore_from_trash</i>
                        </button>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'nama_jenis' => $request->nama_jenis
        );

        $insert = Jenis::updateOrCreate(
            [
                'id' => $request->id
            ],
            $data
        );

        if($insert){
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );
        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Jenis::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $delete = Jenis::find($request->id)->delete();

        if($delete)
        {
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );

        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }
}
