<?php

namespace App\Http\Controllers\Admin\Master\Bank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Bank;
use DataTables;

class BankController extends Controller
{
    public function index()
    {
        return view('admin.master.bank.index');
    }

    public function getdata()
    {
        $data = Bank::orderby('nama_bank','asc')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<button type="button" class="btn btn-warning btn-sm Edit" data-id="' . $row->id . '">
                            <i class="material-icons">mode_edit_outline</i>
                        </button>
                        ';
                return $btn;
            })
            ->addColumn('status', function ($row) {
                if($row->status == 0){
                    $status = 'Non Aktif';
                }else{
                    $status = 'Aktif';
                }
                return $status;
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'nama_bank' => $request->nama_bank,
            'no_rek' => $request->no_rek,
            'status' => $request->status
        );

        $insert = Bank::updateOrCreate(
            [
                'id' => $request->id
            ],
            $data
        );

        if($insert){
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );
        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Bank::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
    }
}
