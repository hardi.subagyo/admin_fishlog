<?php

namespace App\Http\Controllers\Admin\Katalog\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Grade;
use App\Models\Master\Jenis;
use App\Models\Katalog\Produk;
use Validator;
use DataTables;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $grade = Grade::where('jenis_id',$id)->get();
        $jenis = Jenis::find($id);

        return view('admin.katalog.produk.index', compact(['grade','jenis']));
    }

    public function getdata($id)
    {
        $data = Produk::with('grade')->where('jenis_id',$id)->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<button type="button" class="btn btn-warning btn-sm Edit" data-id="' . $row->id . '">
                            <i class="material-icons">mode_edit_outline</i>
                        </button>
                        <button type="button" class="btn btn-sm  btn-danger Delete" data-id="' . $row->id . '">
                            <i class="material-icons">restore_from_trash</i>
                        </button>
                        ';
                return $btn;
            })
            ->addColumn('grade', function ($row) {
                return $row->grade->nama_grade;
            })
            ->addColumn('harga', function ($row) {
                return number_format($row->harga,0,'.','.');
            })
            ->addColumn('foto', function ($row) {
                $foto = '<img src="'.asset("uploads/foto/produk/".$row->foto."").'" class="img-fluid" width="100%">';
                return $foto;
            })
             ->addColumn('video', function ($row) {
                if($row->video == ''){
                    $video = '';
                }else{
                    $video = '<video width="100%" controls><source src="'.asset("uploads/video/produk/".$row->video."").'" type="video/mp4"></video>';   
                }
                
                return $video;
            })
            ->rawColumns(['action','grade','harga','foto','video'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'foto' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            'video' => 'nullable|file|mimetypes:video/mp4',
        ]);

        if ($validator->fails()) {
            $output = array(
                'state' => '500',
                'msg' => $validator->errors()->first(),
            );
        }else{

            $data = array(
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'harga' => str_replace('.','',$request->harga),
                'grade_id' => $request->grade_id,
                'jenis_id' => $request->jenis_id
            );

            if ($request->file('foto') != null) {

                if($request->id != ''){
                    $getdata = Produk::find($request->id);
                    if($getdata->foto != ""){
                        File::delete(public_path("uploads/foto/produk/".$getdata->foto));
                    }
                }

                $foto = date('ymdhis').'-'.str_replace(" ","_", $request->file('foto')->getClientOriginalName());
                $request->file('foto')->move(public_path('uploads/foto/produk'), $foto);
                $data['foto'] = $foto;
            }


            if ($request->file('video') != null) {

                if($request->id != ''){
                    $getdata = Produk::find($request->id);
                    if($getdata->video != ""){
                        File::delete(public_path("uploads/video/produk/".$getdata->video));
                    }
                }

                $video = date('ymdhis').'-'.str_replace(" ","_", $request->file('video')->getClientOriginalName());
                $request->file('video')->move(public_path('uploads/video/produk'), $video);
                $data['video'] = $video;
            }

            $insert = Produk::updateOrCreate(
                [
                    'id' => $request->id
                ],
                $data
            );

            if($insert){
                $output = array(
                    'state' => 200,
                    'msg' => 'Sukses'
                );
            }else{
                $output = array(
                    'state' => 500,
                    'msg' => 'Gagal'
                );
            }

        }

        /*$output = array(
            'state' => '500',
            'msg' => $request->file('foto')->getClientOriginalName()
        );*/

        echo json_encode($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Produk::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = Produk::find($request->id);

        File::delete(public_path("uploads/foto/produk/".$data->foto));
        File::delete(public_path("uploads/video/produk/".$data->video));

        $delete = Produk::find($request->id)->delete();

        if($delete)
        {
            $output = array(
                'state' => 200,
                'msg' => 'Sukses'
            );

        }else{
            $output = array(
                'state' => 500,
                'msg' => 'Gagal'
            );
        }

        echo json_encode($output);
    }
}
