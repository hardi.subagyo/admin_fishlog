<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Jenis;
use App\Models\Katalog\Produk;
use DB;
use Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr_jenis = array();
        $jenis = Jenis::all();
        foreach($jenis as $item){
            $produk = Produk::where('jenis_id',$item->id)->count();
            array_push($arr_jenis,array(
                'jenis' => $item->nama_jenis,
                'count' => $produk
            ));
        }

        $transaksi = DB::table('trx_checkout as trx')
                    ->select('trx.*','u.name','u.no_telp')
                    ->join('users as u','u.id','=','trx.users_id')
                    ->orderBy('trx.id','desc')
                    ->limit(5)
                    ->get();

        return view('admin.dashboard.index',compact(['arr_jenis','transaksi']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
