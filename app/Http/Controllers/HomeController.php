<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Jenis;

class HomeController extends Controller
{
    public static function jenis()
    {
        $data = Jenis::orderby('nama_jenis','asc')->get();

        return $data;
    }
}
